package kz.iqlabs.upartyapp.main;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.chat.ChatListFragment;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.orders.OrderDetailsFragment;
import kz.iqlabs.upartyapp.orders.OrderFragment;
import kz.iqlabs.upartyapp.orders.ResponseFragment;
import kz.iqlabs.upartyapp.orders.models.OrderModel;
import kz.iqlabs.upartyapp.orders.models.ResponseModel;
import kz.iqlabs.upartyapp.profile.ArtistProfileFragment;
import kz.iqlabs.upartyapp.profile.EmployerProfileFragment;
import kz.iqlabs.upartyapp.profile.MyArtistProfileFragment;

public class MainActivity extends AppCompatActivity implements OrderFragment.OnListFragmentInteractionListener, ResponseFragment.OnListFragmentInteractionListener,
        MyArtistProfileFragment.OnFragmentInteractionListener, OrderDetailsFragment.OnFragmentInteractionListener {

    @BindView(R.id.nav_view)
    BottomNavigationView navView;

    public boolean isEmployee;
    FragmentManager manager;

    UserModel userModel;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {

        switch (item.getItemId()) {
            case R.id.orders:
                setTitle("Orders");
                manager.beginTransaction().replace(R.id.content, OrderFragment.newInstance()).commit();
                return true;
            case R.id.responses:
                setTitle("Responses");
                manager.beginTransaction().replace(R.id.content, OrderFragment.newInstance()).commit();
                return true;
            case R.id.chat:
                setTitle("Chats");
                manager.beginTransaction().replace(R.id.content, ChatListFragment.newInstance()).commit();
                return true;
            case R.id.profile:
                setTitle("My profile");
                if (isEmployee)
                    manager.beginTransaction().replace(R.id.content, MyArtistProfileFragment.newInstance()).commit();
                else
                    manager.beginTransaction().replace(R.id.content, EmployerProfileFragment.newInstance()).commit();
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        manager = getSupportFragmentManager();
        OrderFragment fragment = OrderFragment.newInstance();
        manager.beginTransaction().replace(R.id.content, fragment).commit();
        setTitle("Orders");

        String userStr = getSharedPreferences("", Context.MODE_PRIVATE).getString("user","");
        userModel = new Gson().fromJson(userStr,UserModel.class);

        if (userModel.getRole().equals("employer")) {
            isEmployee = false;
            navView.getMenu().removeItem(R.id.responses);
        } else {
            isEmployee = true;
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        Log.d("getBackStackEntryCount", String.valueOf(fm.getBackStackEntryCount()));
        if (fm.getBackStackEntryCount() > 0) {
            Log.d("getBackStackEntryCount", String.valueOf(fm.getBackStackEntryCount()));
            super.onBackPressed();
        } else {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Close app?");
            builder.setPositiveButton("Yes", (dialogInterface, i) -> finishAffinity());
            builder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss());
            builder.show();

        }
    }



    @Override
    public void orderListItemClick(OrderModel item) {
        if(!isEmployee) {
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content, ResponseFragment.newInstance(1)).addToBackStack("Response").commit();
        } else {
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.content, OrderDetailsFragment.newInstance(null)).addToBackStack("Order details").commit();
        }
    }

    @Override
    public void responseListItemClick(ResponseModel item) {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.content, ArtistProfileFragment.newInstance(String.valueOf(item.getId()))).addToBackStack("Artist").commit();
    }

    @Override
    public void myArtistProfileInteraction(Uri uri) {

    }

    @Override
    public void onItemClick(Uri uri) {

    }
}
