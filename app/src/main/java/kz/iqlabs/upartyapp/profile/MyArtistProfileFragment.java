package kz.iqlabs.upartyapp.profile;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.login.LoginActivity;
import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.orders.api.GetUserInfoService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyArtistProfileFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    public static MyArtistProfileFragment newInstance() {
        MyArtistProfileFragment fragment = new MyArtistProfileFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.categories)
    TextView categories;
    @BindView(R.id.avatar)
    CircleImageView avatar;
    @BindView(R.id.rating)
    TextView rating;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View myView = inflater.inflate(R.layout.fragment_my_artist_profile, container, false);
        ButterKnife.bind(this,myView);
        init();
        return myView;
    }


    private void init() {
        loadUserInfo();
    }

    private void loadUserInfo() {
        String userStr = getContext().getSharedPreferences("", Context.MODE_PRIVATE).getString("user","");
        UserModel userModel = new Gson().fromJson(userStr,UserModel.class);
        String user_id = String.valueOf(userModel.getId());
        GetUserInfoService getUserInfoService = ApiClient.getClient().create(GetUserInfoService.class);
        String url = "http://iqlabs.kz/uparty/public/api/user/info/"+user_id;
        getUserInfoService.getUserInfo(url).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Log.e("get_artist_info", String.valueOf(response.isSuccessful()));
                if(response.errorBody()!=null) {
                    try {
                        Log.e("get_artist_info", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if(response.isSuccessful()) {
                    UserModel artist = response.body().getUserModel();
                    name.setText(artist.getName());
                    rating.setText(artist.getRating());
                    email.setText(artist.getEmail());
                    phone.setText("+1"+artist.getPhone());
                    Picasso.get().load(artist.getAvatar()).into(avatar);
                    String categoriesStr = "";
                    for (int i = 0; i < artist.getCategoryModels().size(); i++) {
                        if (i == artist.getCategoryModels().size() - 1)
                            categoriesStr += artist.getCategoryModels().get(i).getEnName();
                        else categoriesStr += artist.getCategoryModels().get(i).getEnName() + ", ";
                    }
                    categories.setText(categoriesStr);

                    String json = new Gson().toJson(artist);
                    SharedPreferences sharedPreferences = getContext().getSharedPreferences("",0);
                    sharedPreferences.edit().putString("user",json).apply();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Snackbar.make(name,"Network failure",Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.myArtistProfileInteraction(uri);
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.edit)
    void editClick() {
        startActivity(new Intent(getActivity(),EditArtistActivity.class));
    }
    @OnClick(R.id.logout)
    void logOut() {
        startActivity(new Intent(getActivity(), LoginActivity.class));
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void myArtistProfileInteraction(Uri uri);
    }
}
