package kz.iqlabs.upartyapp.profile;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.chat.ChatService;
import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.orders.api.GetUserInfoService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArtistProfileFragment extends Fragment {

    public static ArtistProfileFragment newInstance(String user_id) {
        ArtistProfileFragment fragment = new ArtistProfileFragment();
        Bundle args = new Bundle();
        args.putString("user_id", user_id);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.avatar)
    CircleImageView avatar;
    @BindView(R.id.rat)
    TextView rating;
    @BindView(R.id.phone)
    TextView phone;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.categories)
    TextView categories;
    @BindView(R.id.about)
    TextView about;

    private UserModel artist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myView = inflater.inflate(R.layout.fragment_artist_profile, container, false);
        ButterKnife.bind(this, myView);
        init();
        return myView;
    }

    private void init() {
        Typeface akroblack = Typeface.createFromAsset(getActivity().getAssets(), "fonts/akroblack.otf");
        Typeface akrolight = Typeface.createFromAsset(getActivity().getAssets(), "fonts/akrolight.otf");
        name.setTypeface(akroblack);
        categories.setTypeface(akrolight);
        about.setTypeface(akrolight);
        loadArtistInfo();
    }


    private void loadArtistInfo() {
        String user_id = getArguments().getString("user_id");
        GetUserInfoService getOrdersService = ApiClient.getClient().create(GetUserInfoService.class);
        String url = "http://iqlabs.kz/uparty/public/api/user/info/" + user_id;
        Log.e("artist_url", url);
        getOrdersService.getUserInfo(url).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Log.e("get_employer_ord", String.valueOf(response.isSuccessful()));
                if (response.errorBody() != null) {
                    try {
                        Log.e("get_employer_ord", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (response.isSuccessful()) {
                    artist = response.body().getUserModel();
                    name.setText(artist.getName());
                    about.setText(artist.getAbout());
                    Picasso.get().load(artist.getAvatar()).into(avatar);
                    phone.setText("+1" + artist.getPhone());
                    rating.setText(String.valueOf(artist.getRating()));
                    String categoriesStr = "";
                    for (int i = 0; i < artist.getCategoryModels().size(); i++) {
                        if (i == artist.getCategoryModels().size() - 1)
                            categoriesStr += artist.getCategoryModels().get(i).getEnName();
                        else categoriesStr += artist.getCategoryModels().get(i).getEnName() + ", ";
                    }
                    categories.setText(categoriesStr);
                }


            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Snackbar.make(name, "Network failure", Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @OnClick(R.id.call)
    void startCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:19293304441"));
        startActivity(intent);
    }

    @OnClick(R.id.chat)
    void startChat() {
        Snackbar.make(name, "You will be redirected to whatsapp", Snackbar.LENGTH_SHORT).show();
        addChat();
    }


    private void addChat() {
        String userStr = getContext().getSharedPreferences("", Context.MODE_PRIVATE).getString("user", "");
        UserModel userModel = new Gson().fromJson(userStr, UserModel.class);
        ChatService chatService = ApiClient.getClient().create(ChatService.class);
        chatService.addNewChat(userModel.getId(), artist.getId()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Log.e("add_chat_resp", String.valueOf(response.isSuccessful()));
                if (response.errorBody() != null) {
                    try {
                        Log.e("add_chat_resp", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (response.isSuccessful()) {
                    String whatsappApi = "https://wa.me/" + artist.getPhone();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(whatsappApi));
                    startActivity(browserIntent);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Snackbar.make(name, "Network failure", Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @OnClick(R.id.instagram)
    void instaClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(artist.getInstagram()));
        startActivity(browserIntent);
    }

    @OnClick(R.id.facebook)
    void facebookClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(artist.getFacebook()));
        startActivity(browserIntent);
    }

    @OnClick(R.id.youtube)
    void youtubeClick() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(artist.getTwitter()));
        startActivity(browserIntent);
    }

    @OnClick(R.id.decline)
    void declineArtist() {
        getActivity().onBackPressed();
    }

}
