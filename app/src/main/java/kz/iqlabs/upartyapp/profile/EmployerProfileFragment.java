package kz.iqlabs.upartyapp.profile;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.login.LoginActivity;
import kz.iqlabs.upartyapp.login.model.UserModel;

public class EmployerProfileFragment extends Fragment {


    // TODO: Rename and change types and number of parameters
    public static EmployerProfileFragment newInstance() {
        EmployerProfileFragment fragment = new EmployerProfileFragment();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.avatar)
    CircleImageView avatar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myView = inflater.inflate(R.layout.fragment_employer_profile, container, false);
        ButterKnife.bind(this, myView);
        init();
        return myView;
    }

    private void init() {
        Typeface akroblack = Typeface.createFromAsset(getActivity().getAssets(), "fonts/akroblack.otf");
        Typeface akrolight = Typeface.createFromAsset(getActivity().getAssets(), "fonts/akrolight.otf");

        name.setTypeface(akroblack);
        email.setTypeface(akrolight);
        phone.setTypeface(akrolight);

        String userStr = getContext().getSharedPreferences("", Context.MODE_PRIVATE).getString("user","");
        UserModel userModel = new Gson().fromJson(userStr,UserModel.class);
        name.setText(userModel.getName());
        email.setText(userModel.getEmail());
        phone.setText("+1"+userModel.getPhone());
        Picasso.get().load(userModel.getAvatar()).into(avatar);

    }

    @OnClick(R.id.log_out)
    void logOut() {
        startActivity(new Intent(getActivity(), LoginActivity.class));
    }
}
