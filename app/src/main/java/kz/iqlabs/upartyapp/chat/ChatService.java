package kz.iqlabs.upartyapp.chat;

import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.orders.models.MyResponseResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ChatService {

    @GET("chat/list")
    Call<ChatResponse> getChatList(@Query("user_id") int user_id);

    @POST("chat/add")
    @FormUrlEncoded
    Call<LoginResponse> addNewChat(@Field("first") int user_id, @Field("second") int artist_id);
}
