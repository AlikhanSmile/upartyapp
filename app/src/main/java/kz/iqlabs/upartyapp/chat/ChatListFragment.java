package kz.iqlabs.upartyapp.chat;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.gms.common.api.Api;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.orders.models.MyResponseResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatListFragment extends Fragment {


    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.empty)
    LinearLayout empty;

    public static ChatListFragment newInstance() {
        ChatListFragment fragment = new ChatListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myView = inflater.inflate(R.layout.fragment_chat_list, container, false);
        ButterKnife.bind(this, myView);
        init();
        return myView;
    }

    private void init() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        loadChats();
    }

    private void loadChats() {
        ChatService chatService = ApiClient.getClient().create(ChatService.class);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        String userStr = getContext().getSharedPreferences("", Context.MODE_PRIVATE).getString("user", "");
        UserModel userModel = new Gson().fromJson(userStr, UserModel.class);

        chatService.getChatList(userModel.getId()).enqueue(new Callback<ChatResponse>() {
            @Override
            public void onResponse(Call<ChatResponse> call, Response<ChatResponse> response) {
                progressBar.setVisibility(View.GONE);
                Log.e("get_chat_list", String.valueOf(response.isSuccessful()));
                if (response.errorBody() != null) {
                    try {
                        Log.e("get_chat_list", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (response.isSuccessful()) {
                    if (response.body().getData().size() == 0) empty.setVisibility(View.VISIBLE);
                    else {
                        recyclerView.setVisibility(View.VISIBLE);
                        ArrayList<ChatResponse.ChatBody> chatBodies = response.body().getData();
                        ChatListAdapter adapter = new ChatListAdapter(chatBodies,getContext());
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        recyclerView.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(recyclerView, "Network failure", Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });

    }

}
