package kz.iqlabs.upartyapp.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import kz.iqlabs.upartyapp.orders.models.MyResponseBody;

public class ChatResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private ArrayList<ChatBody> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<ChatBody> getData() {
        return data;
    }

    public void setData(ArrayList<ChatBody> data) {
        this.data = data;
    }


    class ChatBody {
        @SerializedName("id")
        @Expose
        private Integer id;

        @SerializedName("second_user")
        @Expose
        private SecondUser secondUser;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public SecondUser getSecondUser() {
            return secondUser;
        }

        public void setSecondUser(SecondUser secondUser) {
            this.secondUser = secondUser;
        }
    }


    public class SecondUser {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("avatar")
        @Expose
        private String avatar;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }
}
