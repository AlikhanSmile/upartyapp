package kz.iqlabs.upartyapp.chat;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.main.MainActivity;
import kz.iqlabs.upartyapp.profile.ArtistProfileFragment;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ViewHolder> {

    private ArrayList<ChatResponse.ChatBody> chatBodies;

    Context context;
    public ChatListAdapter(ArrayList<ChatResponse.ChatBody> chatBodies, Context context) {
        this.chatBodies = chatBodies;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_model,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.chatBody = chatBodies.get(position);
        Picasso.get().load(holder.chatBody.getSecondUser().getAvatar()).into(holder.avatar);
        holder.name.setText(holder.chatBody.getSecondUser().getName());
    }

    @Override
    public int getItemCount() {
        return chatBodies.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.avatar)
        CircleImageView avatar;
        @BindView(R.id.name)
        TextView name;
        final View mView;

        ChatResponse.ChatBody chatBody;

        ViewHolder(@NonNull View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this,mView);
            mView.setOnClickListener(v -> {
                MainActivity activity = (MainActivity) context;
                FragmentManager manager = activity.getSupportFragmentManager();
                manager.beginTransaction().replace(R.id.content, ArtistProfileFragment.newInstance(String.valueOf(chatBody.getSecondUser().getId()))).addToBackStack("Artist").commit();
//                String whatsappApi = "https://wa.me/"+phone;
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(whatsappApi));
//                context.startActivity(browserIntent);
            });

        }
    }
}
