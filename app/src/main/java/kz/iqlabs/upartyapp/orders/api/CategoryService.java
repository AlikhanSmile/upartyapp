package kz.iqlabs.upartyapp.orders.api;

import kz.iqlabs.upartyapp.orders.models.CategoryResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CategoryService {
    @GET("category/list")
    Call<CategoryResponse> getCategory(@Query("parent_id") int parent_id);

}
