package kz.iqlabs.upartyapp.orders;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.orders.adapter.ResponseAdapter;
import kz.iqlabs.upartyapp.orders.api.MyResponseService;
import kz.iqlabs.upartyapp.orders.models.MyResponseBody;
import kz.iqlabs.upartyapp.orders.models.MyResponseResponse;
import kz.iqlabs.upartyapp.orders.models.ResponseModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class ResponseFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ResponseFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static ResponseFragment newInstance(int columnCount) {
        ResponseFragment fragment = new ResponseFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_response_list, container, false);
        getActivity().setTitle("Responses");
        ButterKnife.bind(this, view);

        init();
        return view;
    }

    private void init() {
        loadResponses();
    }

    private void loadResponses() {
        String userStr = getContext().getSharedPreferences("", Context.MODE_PRIVATE).getString("user", "");
        UserModel userModel = new Gson().fromJson(userStr, UserModel.class);
        String user_id = String.valueOf(userModel.getId());
        MyResponseService myResponseService = ApiClient.getClient().create(MyResponseService.class);

        myResponseService.getEmployerResponses(Integer.parseInt(user_id)).enqueue(new Callback<MyResponseResponse>() {
            @Override
            public void onResponse(Call<MyResponseResponse> call, Response<MyResponseResponse> response) {
                Log.e("get_employer_resp", String.valueOf(response.isSuccessful()));
                if(response.errorBody()!=null) {
                    try {
                        Log.e("get_employer_resp", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if(response.isSuccessful()) {
                    ArrayList<MyResponseBody> responseBodies = response.body().getData();
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    ArrayList<ResponseModel> responseModels = new ArrayList<>();
                    for(int i = 0 ; i < responseBodies.size(); i++) {
                        responseModels.add(responseBodies.get(i).getResponseModel());
                    }
                    recyclerView.setAdapter(new ResponseAdapter(responseModels, mListener));
                }
            }

            @Override
            public void onFailure(Call<MyResponseResponse> call, Throwable t) {
                Snackbar.make(recyclerView, "Network failure", Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void responseListItemClick(ResponseModel item);
    }
}
