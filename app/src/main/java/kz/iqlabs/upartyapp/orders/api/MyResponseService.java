package kz.iqlabs.upartyapp.orders.api;

import kz.iqlabs.upartyapp.orders.models.MyResponseResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MyResponseService {
    @GET("response/list")
    Call<MyResponseResponse> getEmployerResponses(@Query("user_id") int user_id);
}
