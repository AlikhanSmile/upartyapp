package kz.iqlabs.upartyapp.orders.adapter;

import kz.iqlabs.upartyapp.common.CategoryModel;

public interface RecyclerListener {
    void itemClicked(CategoryModel model);
}
