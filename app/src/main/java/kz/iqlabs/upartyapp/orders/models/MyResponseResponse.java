package kz.iqlabs.upartyapp.orders.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyResponseResponse {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private ArrayList<MyResponseBody> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public ArrayList<MyResponseBody> getData() {
        return data;
    }

    public void setData(ArrayList<MyResponseBody> data) {
        this.data = data;
    }
}

