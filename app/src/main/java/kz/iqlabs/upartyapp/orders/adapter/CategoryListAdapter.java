package kz.iqlabs.upartyapp.orders.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.common.CategoryModel;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder>{

    private final ArrayList<CategoryModel> mValues;

    private Context context;

    private RecyclerListener listener;

    public CategoryListAdapter(ArrayList<CategoryModel> items, RecyclerListener listener) {
        mValues = items;
        this.listener = listener;
        this.context = (Context)listener;
    }

    public void addModel(CategoryModel model) {
        mValues.add(model);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        if(holder.mItem.getEnName()!=null) holder.name.setText(holder.mItem.getEnName());
        holder.delete.setOnClickListener(v -> {
            mValues.remove(holder.mItem);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.category)
        public EditText name;

        @BindView(R.id.delete)
        ImageView delete;

        public CategoryModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this,mView);
            mView.setOnClickListener(v -> {
                listener.itemClicked(mItem);
            });
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "'";
        }
    }
}
