package kz.iqlabs.upartyapp.orders.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import kz.iqlabs.upartyapp.common.CategoryModel;
import kz.iqlabs.upartyapp.login.model.UserModel;

public class OrderModel implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("en_topic")
    @Expose
    private String enTopic;
    @SerializedName("es_topic")
    @Expose
    private String esTopic;
    @SerializedName("ru_topic")
    @Expose
    private String ruTopic;
    @SerializedName("en_description")
    @Expose
    private Object enDescription;
    @SerializedName("es_description")
    @Expose
    private Object esDescription;
    @SerializedName("ru_description")
    @Expose
    private Object ruDescription;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("date")
    @Expose
    private Long date;
    @SerializedName("price_from")
    @Expose
    private Integer priceFrom;
    @SerializedName("price_to")
    @Expose
    private Integer priceTo;
    @SerializedName("available")
    @Expose
    private Integer available;
    @SerializedName("user")
    @Expose
    private UserModel employer;
    @SerializedName("categories")
    @Expose
    private ArrayList<CategoryModel> categoryModels;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnTopic() {
        return enTopic;
    }

    public void setEnTopic(String enTopic) {
        this.enTopic = enTopic;
    }

    public String getEsTopic() {
        return esTopic;
    }

    public void setEsTopic(String esTopic) {
        this.esTopic = esTopic;
    }

    public String getRuTopic() {
        return ruTopic;
    }

    public void setRuTopic(String ruTopic) {
        this.ruTopic = ruTopic;
    }

    public Object getEnDescription() {
        return enDescription;
    }

    public void setEnDescription(Object enDescription) {
        this.enDescription = enDescription;
    }

    public Object getEsDescription() {
        return esDescription;
    }

    public void setEsDescription(Object esDescription) {
        this.esDescription = esDescription;
    }

    public Object getRuDescription() {
        return ruDescription;
    }

    public void setRuDescription(Object ruDescription) {
        this.ruDescription = ruDescription;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Integer getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(Integer priceFrom) {
        this.priceFrom = priceFrom;
    }

    public Integer getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(Integer priceTo) {
        this.priceTo = priceTo;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public String getPriceDisplay() {
        return "$"+getPriceFrom()+" - "+"$"+getPriceTo();
    }
    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    private String getDateDisplay() {
        return getDate(getDate());
    }

}