package kz.iqlabs.upartyapp.orders.api;

import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.orders.models.CategoryResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AddOrderService {
    @POST("offer/add")
    @FormUrlEncoded
    Call<CategoryResponse>  addOrder(@Field("en_topic") String en_topic,
                                 @Field("en_description") String en_description,
                                 @Field("state") String state,
                                 @Field("city") String city,
                                 @Field("address") String address,
                                 @Field("latitude") double latitude,
                                 @Field("longitude") double longitude,
                                 @Field("date") String date,
                                 @Field("price_from") String price_from,
                                 @Field("price_to") String price_to,
                                 @Field("user_id") int user_id,
                                 @Field("categories") String categories);

}
