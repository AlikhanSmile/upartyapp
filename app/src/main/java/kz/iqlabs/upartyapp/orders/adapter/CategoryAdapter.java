package kz.iqlabs.upartyapp.orders.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.common.CategoryModel;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private final ArrayList<CategoryModel> mValues;

    private Context context;

    private RecyclerListener listener;

    public CategoryAdapter(ArrayList<CategoryModel> items, RecyclerListener listener) {
        mValues = items;
        this.listener = listener;
        this.context = (Context)listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_model, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.name.setText(mValues.get(position).getEnName());
        Typeface akronorm = Typeface.createFromAsset(context.getAssets(), "fonts/akronorm.otf");
        holder.name.setTypeface(akronorm);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.name)
        public TextView name;

        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;

        public CategoryModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this,mView);
            mView.setOnClickListener(v -> {
                listener.itemClicked(mItem);
                mainLayout.setBackgroundColor(context.getResources().getColor(R.color.colorNavyBlue));
            });
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "'";
        }
    }
}
