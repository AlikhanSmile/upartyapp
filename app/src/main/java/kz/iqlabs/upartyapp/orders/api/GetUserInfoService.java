package kz.iqlabs.upartyapp.orders.api;

import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.orders.models.CategoryResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface GetUserInfoService {
    @GET
    Call<LoginResponse> getUserInfo(@Url String url);

}
