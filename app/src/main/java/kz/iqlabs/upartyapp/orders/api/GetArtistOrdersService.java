package kz.iqlabs.upartyapp.orders.api;

import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.orders.models.OrderResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface GetArtistOrdersService {

    @GET("offer/list")
    Call<OrderResponse> getArtistOrders(@Query("user_id") String user_id);
}
