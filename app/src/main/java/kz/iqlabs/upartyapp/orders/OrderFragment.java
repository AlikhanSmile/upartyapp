package kz.iqlabs.upartyapp.orders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.main.MainActivity;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.orders.adapter.OrderAdapter;
import kz.iqlabs.upartyapp.orders.adapter.SimpleItemTouchHelperCallback;
import kz.iqlabs.upartyapp.orders.api.GetArtistOrdersService;
import kz.iqlabs.upartyapp.orders.api.GetUserInfoService;
import kz.iqlabs.upartyapp.orders.models.OrderModel;
import kz.iqlabs.upartyapp.orders.models.OrderResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderFragment extends Fragment {


    OnListFragmentInteractionListener mListener;
    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.empty)
    LinearLayout empty;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    private String user_id;

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ordermodel_list, container, false);
        ButterKnife.bind(this, view);
        // Set the adapter
        //getActivity().setTitle("Orders");
        Context context = view.getContext();
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        init();


        return view;
    }

    private void init() {
        String userStr = getContext().getSharedPreferences("", Context.MODE_PRIVATE).getString("user","");
        UserModel userModel = new Gson().fromJson(userStr,UserModel.class);
        user_id = String.valueOf(userModel.getId());

        loadOrders(userModel.getRole());


    }

    private void loadOrders(String role) {
        empty.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        if(role.equals("employer"))
            loadEmployerOrders();
        else
            loadArtistOrders();
    }



    private void loadArtistOrders() {
        GetArtistOrdersService getArtistOrdersService = ApiClient.getClient().create(GetArtistOrdersService.class);
        getArtistOrdersService.getArtistOrders(user_id).enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                progressBar.setVisibility(View.GONE);
                Log.e("loadArtistOrders", String.valueOf(response.isSuccessful()));
                if(response.errorBody()!=null) {
                    try {
                        Log.e("loadArtistOrders", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if(response.isSuccessful()) {
                    if(response.body().getData().size()>0) {
                        ArrayList<OrderModel> orderModels = response.body().getData();
                        MainActivity activity = (MainActivity) getActivity();
                        OrderAdapter adapter = new OrderAdapter(orderModels,activity);
                        recyclerView.setAdapter(adapter);
                    } else {
                        empty.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(recyclerView,"Network failure", Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    private void loadEmployerOrders() {

        GetUserInfoService getOrdersService = ApiClient.getClient().create(GetUserInfoService.class);
        String url = "http://iqlabs.kz/uparty/public/api/user/info/"+user_id;
        getOrdersService.getUserInfo(url).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressBar.setVisibility(View.GONE);
                Log.e("get_employer_ord", String.valueOf(response.isSuccessful()));
                if(response.errorBody()!=null) {
                    try {
                        Log.e("get_employer_ord", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if(response.isSuccessful()){
                    if (response.body().getUserModel().getOffers().size()>0) {
                        ArrayList<OrderModel> orderModels = response.body().getUserModel().getOffers();
                        OrderAdapter adapter = new OrderAdapter(orderModels, mListener);
                        recyclerView.setAdapter(adapter);
                        ItemTouchHelper.Callback callback =
                                new SimpleItemTouchHelperCallback(adapter);
                        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                        touchHelper.attachToRecyclerView(recyclerView);
                    } else {
                        empty.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Snackbar.make(recyclerView,"Network failure", Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void orderListItemClick(OrderModel item);
    }

    @OnClick(R.id.add_order)
    void addOrder() {
        getActivity().startActivity(new Intent(getContext(), AddOrderActivity.class));
    }
}
