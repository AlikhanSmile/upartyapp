package kz.iqlabs.upartyapp.orders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.common.CategoryModel;
import kz.iqlabs.upartyapp.common.DatePickerFragment;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.main.MainActivity;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.orders.adapter.CategoryListAdapter;
import kz.iqlabs.upartyapp.orders.adapter.RecyclerListener;
import kz.iqlabs.upartyapp.orders.api.AddOrderService;
import kz.iqlabs.upartyapp.orders.models.CategoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddOrderActivity extends AppCompatActivity implements RecyclerListener {

    @BindView(R.id.topic)
    EditText topic;
    @BindView(R.id.date)
    EditText date;
    @BindView(R.id.price_from)
    EditText price_from;
    @BindView(R.id.price_to)
    EditText price_to;
    @BindView(R.id.city)
    EditText city;
    @BindView(R.id.address)
    EditText address;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.finish)
    Button btn_finish;

    @BindView(R.id.category_container)
    LinearLayout container;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    String categories = "";
    int user_id = 0;
    String dateStr = "";

    ArrayList<CategoryModel> pickedCategories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_order);
        ButterKnife.bind(this);
        setTitle("Add order");
        init();
    }

    private void init() {
        pickedCategories = new ArrayList<>();
        String userStr = getSharedPreferences("", Context.MODE_PRIVATE).getString("user","");
        UserModel userModel = new Gson().fromJson(userStr,UserModel.class);
        user_id = userModel.getId();

    }



    @OnClick(R.id.add)
    void addCategory() {
        Intent intent = new Intent(this,CategoryActivity.class);
        startActivityForResult(intent,1);
    }

    void concatCategory(int id) {
        categories += ",{\"id\":" + id + ",\"solo\":true}";
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data != null) {

                CategoryModel model = (CategoryModel) data.getExtras().get("category");
                pickedCategories.add(model);

                LinearLayout catSource = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.category_layout, null);
                container.addView(catSource);
                EditText name = catSource.findViewById(R.id.category);
                ImageView delete = catSource.findViewById(R.id.delete);
                name.setText(model.getEnName());
                delete.setOnClickListener(v -> {
                    pickedCategories.remove(model);
                    container.removeView(catSource);
                });



            }

        }
    }

    @OnClick(R.id.finish)
    void uploadOrder() {
        categories = "";
        setupCategory();
        if (checkFields()) {
            doUpload();
        } else {
            Snackbar.make(topic, "All fields should be filled", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void setupCategory() {
        for(int i = 0; i < pickedCategories.size(); i++) {
            if(i==0) categories += "{\"id\":" + pickedCategories.get(i).getId() + ",\"solo\":true}";
            else concatCategory(pickedCategories.get(i).getId());
        }
        categories = "[" + categories + "]";
    }


    private boolean checkFields() {
        return !topic.getText().toString().isEmpty() && !date.getText().toString().isEmpty() && !price_from.getText().toString().isEmpty()
                && !price_to.getText().toString().isEmpty() && !city.getText().toString().isEmpty() && !address.getText().toString().isEmpty()
                && !description.getText().toString().isEmpty() && !categories.isEmpty();
    }

    void doUpload() {
        progressBar.setVisibility(View.VISIBLE);
        btn_finish.setVisibility(View.GONE);
        Log.e("date", dateStr);
        AddOrderService addOrderService = ApiClient.getClient().create(AddOrderService.class);
        addOrderService.addOrder(topic.getText().toString(), description.getText().toString(), "NY", city.getText().toString(), address.getText().toString(),
                10.385, 10.385, dateStr, price_from.getText().toString(), price_to.getText().toString(), user_id, categories).enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                progressBar.setVisibility(View.GONE);
                btn_finish.setVisibility(View.VISIBLE);
                Log.e("add_order", String.valueOf(response.isSuccessful()));
                if (response.errorBody() != null) {
                    try {
                        Log.e("add_order", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        Snackbar.make(description, "Order successfully created!", Snackbar.LENGTH_SHORT).show();
                        startActivity(new Intent(AddOrderActivity.this, MainActivity.class));
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btn_finish.setVisibility(View.VISIBLE);
                Snackbar.make(description, "Network failure", Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @OnClick(R.id.date)
    void showDatePicker() {
        DatePickerFragment date = new DatePickerFragment();
        /**
         * Set Up Current Date Into dialog
         */
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("year", calender.get(Calendar.YEAR));

        date.setArguments(args);
        /**
         * Set Call back to capture selected date
         */
        date.setCallBack(ondate);
        date.show(getSupportFragmentManager(), "Date Picker");
    }

    DatePickerDialog.OnDateSetListener ondate = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            String month, day;
            if (monthOfYear + 1 < 10) {
                month = "0" + (monthOfYear + 1);
            } else {
                month = String.valueOf(monthOfYear + 1);
            }

            if (dayOfMonth < 10) {
                day = "0" + dayOfMonth;
            } else {
                day = String.valueOf(dayOfMonth);
            }

            date.setText(year + "-" + month + "-" + day);

            DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Date date = null;
            try {
                date = formatter.parse(day + "-" + month + "-" + year);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String dateStrTemp = String.valueOf(date.getTime());
            dateStr = dateStrTemp.substring(0, dateStrTemp.length() - 3);

        }
    };

    @Override
    public void itemClicked(CategoryModel model) {

    }
}
