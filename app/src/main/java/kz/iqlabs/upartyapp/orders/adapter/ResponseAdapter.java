package kz.iqlabs.upartyapp.orders.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.Circle;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.orders.ResponseFragment.OnListFragmentInteractionListener;
import kz.iqlabs.upartyapp.orders.models.ResponseModel;


public class ResponseAdapter extends RecyclerView.Adapter<ResponseAdapter.ViewHolder> {

    private final ArrayList<ResponseModel> mValues;
    private final OnListFragmentInteractionListener mListener;
    private Context context;
    public ResponseAdapter(ArrayList<ResponseModel> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        context = (Context) listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_response, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);

        holder.city.setText(mValues.get(position).getLocationDisplay());
        holder.name.setText(mValues.get(position).getName());
        Typeface akronorm = Typeface.createFromAsset(context.getAssets(), "fonts/akronorm.otf");
        holder.city.setTypeface(akronorm);
        holder.name.setTypeface(akronorm);
        Picasso.get().load(holder.mItem.getAvatar()).into(holder.avatar);
        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.responseListItemClick(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.avatar)
        CircleImageView avatar;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.city)
        TextView city;
        public ResponseModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this,mView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + name.getText() + "'";
        }
    }
}
