package kz.iqlabs.upartyapp.orders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.common.CategoryModel;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.orders.adapter.CategoryAdapter;
import kz.iqlabs.upartyapp.orders.adapter.RecyclerListener;
import kz.iqlabs.upartyapp.orders.api.CategoryService;
import kz.iqlabs.upartyapp.orders.models.CategoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryActivity extends AppCompatActivity implements RecyclerListener {


    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.topLayout)
    LinearLayout topLayout;
    @BindView(R.id.current_category_label)
    TextView current_category_label;

    CategoryService categoryService;
    CategoryModel pickedCategory;
    ArrayList<CategoryModel> list;
    int parent_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        ButterKnife.bind(this);
        setTitle("Pick category");
        init();
    }

    private void init() {
      loadCategories(0);
    }

    private void loadCategories(int parent_id) {
        categoryService = ApiClient.getClient().create(CategoryService.class);
        categoryService.getCategory(parent_id).enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                Log.e("category_success", String.valueOf(response.isSuccessful()));
                if (response.body() == null) {
                    try {
                        Log.e("category_body", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                list = response.body().getData();
                CategoryAdapter adapter = new CategoryAdapter(list, CategoryActivity.this);
                recyclerView.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Snackbar.make(recyclerView, "Network failure", Snackbar.LENGTH_SHORT).show();
                t.printStackTrace();
            }
        });
    }

    @OnClick(R.id.finish)
    void checkFinish() {
        if (pickedCategory != null) {
            Intent intent = new Intent();
            intent.putExtra("category", pickedCategory);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Snackbar.make(recyclerView, "You should pick category to proceed", Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void itemClicked(CategoryModel model) {
        if(parent_id==0) {
           loadCategories(model.getId());
           topLayout.setVisibility(View.VISIBLE);
            Typeface akronorm = Typeface.createFromAsset(getAssets(), "fonts/akronorm.otf");
            current_category_label.setTypeface(akronorm);
           current_category_label.setText(model.getEnName());
        }
        pickedCategory = model;
    }

}
