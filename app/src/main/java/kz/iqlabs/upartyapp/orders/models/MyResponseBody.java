package kz.iqlabs.upartyapp.orders.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import kz.iqlabs.upartyapp.login.model.UserModel;

public class MyResponseBody {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("offer")
    @Expose
    private OrderModel offer;
    @SerializedName("user")
    @Expose
    private ResponseModel user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public OrderModel getOffer() {
        return offer;
    }

    public void setOffer(OrderModel offer) {
        this.offer = offer;
    }

    public ResponseModel getResponseModel() {
        return user;
    }

    public void setUser(ResponseModel user) {
        this.user = user;
    }
}
