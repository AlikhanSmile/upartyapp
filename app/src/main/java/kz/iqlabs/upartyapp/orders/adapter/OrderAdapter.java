package kz.iqlabs.upartyapp.orders.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.orders.OrderFragment.OnListFragmentInteractionListener;
import kz.iqlabs.upartyapp.orders.models.OrderModel;

/**
 * {@link RecyclerView.Adapter} that can display a {@link OrderModel} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> implements ItemTouchHelperAdapter {

    private final ArrayList<OrderModel> mValues;
    private final OnListFragmentInteractionListener mListener;
    Context context;

    public OrderAdapter(ArrayList<OrderModel> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        context = (Context) listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_ordermodel, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.topic.setText(mValues.get(position).getEnTopic());
        holder.address.setText(mValues.get(position).getAddress());
        holder.price.setText(mValues.get(position).getPriceDisplay());
        Typeface akroblack = Typeface.createFromAsset(context.getAssets(), "fonts/akroblack.otf");
        Typeface akronorm = Typeface.createFromAsset(context.getAssets(), "fonts/akronorm.otf");
        holder.topic.setTypeface(akroblack);
        holder.address.setTypeface(akronorm);
        holder.price.setTypeface(akronorm);

        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                // Notify the active callbacks interface (the activity, if the
                // fragment is attached to one) that an item has been selected.
                mListener.orderListItemClick(holder.mItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(mValues, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(mValues, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);

        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Remove order?");
        OrderModel temp = mValues.get(position);
        mValues.remove(position);
        builder.setPositiveButton("Yes", (dialogInterface, i) -> {
            notifyItemRemoved(position);
        });
        builder.setNegativeButton("No", (dialogInterface, i) ->  {
            dialogInterface.dismiss();
            mValues.add(temp);
            notifyDataSetChanged();
        });
        builder.show();

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;

        @BindView(R.id.topic)
        public TextView topic;
        @BindView(R.id.address)
        public TextView address;
        @BindView(R.id.price)
        public TextView price;

        public OrderModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ButterKnife.bind(this,mView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + topic.getText() + "'";
        }
    }
}
