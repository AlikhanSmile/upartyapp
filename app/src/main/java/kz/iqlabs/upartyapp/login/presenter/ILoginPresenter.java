package kz.iqlabs.upartyapp.login.presenter;

public interface ILoginPresenter {
    void doLogin(String email, String pass);
    void setProgressBarVisiblity(int visibility);
}
