package kz.iqlabs.upartyapp.login.api;

import com.google.gson.JsonObject;

import kz.iqlabs.upartyapp.login.model.ErrorModel;
import kz.iqlabs.upartyapp.login.model.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginService {
    //---User authorization
    @POST("auth/login")
    @FormUrlEncoded
    Call<LoginResponse> makeLogin(@Field("email") String email,
                                  @Field("password") String pass);

}
