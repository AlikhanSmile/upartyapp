package kz.iqlabs.upartyapp.login.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import kz.iqlabs.upartyapp.login.api.LoginService;
import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.login.view.ILoginView;
import kz.iqlabs.upartyapp.network.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements ILoginPresenter {

    private ILoginView iLoginView;
    private LoginService loginService;
    private Context context;
    public LoginPresenter(ILoginView iLoginView) {
        this.iLoginView = iLoginView;
        this.context = (Context) iLoginView;
        loginService = ApiClient.getClient().create(LoginService.class);
    }


    @Override
    public void doLogin(String email, String pass) {
        setProgressBarVisiblity(View.VISIBLE);

        loginService.makeLogin(email,pass).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                setProgressBarVisiblity(View.GONE);
                String errorMessage = null;
                if(response.isSuccessful() && response.body().getSuccess()) {
                    String json = new Gson().toJson(response.body().getUserModel());
                    SharedPreferences sharedPreferences = context.getSharedPreferences("",0);
                    sharedPreferences.edit().putString("user",json).apply();
                } else {
                    errorMessage = response.body().getErrors().get(0).getMessage();
                }
                iLoginView.onLoginResult(response.body().getSuccess(),response.code(),errorMessage);

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                setProgressBarVisiblity(View.GONE);
                iLoginView.onLoginResult(false,404,t.getMessage());
                t.printStackTrace();
            }
        });
    }



    @Override
    public void setProgressBarVisiblity(int visibility) {
        iLoginView.onSetProgressBarVisibility(visibility);
    }
}
