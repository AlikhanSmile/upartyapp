package kz.iqlabs.upartyapp.login;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.login.presenter.LoginPresenter;
import kz.iqlabs.upartyapp.login.view.ILoginView;
import kz.iqlabs.upartyapp.main.MainActivity;
import kz.iqlabs.upartyapp.registration.RegistrationActivity;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText pass;
    @BindView(R.id.fields)
    LinearLayout fields;
    @BindView(R.id.progress)
    ProgressBar progressBar;

    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        loginPresenter = new LoginPresenter(this);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @OnClick(R.id.login)
    void doLogin() {
        hideKeyboard(this);
        if (email.getText().toString().length() > 0 && pass.getText().toString().length() > 0)
            loginPresenter.doLogin(email.getText().toString(), pass.getText().toString());
        else
            Snackbar.make(email, "All fields should be filled", Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.register)
    void goRegister() {
        startActivity(new Intent(this, RegistrationActivity.class));
    }

    @Override
    public void onLoginResult(Boolean result, int code, String errorMessage) {
        if (result) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            Snackbar.make(email, errorMessage, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onSetProgressBarVisibility(int visibility) {
        progressBar.setVisibility(visibility);
        if (visibility == View.VISIBLE) fields.setVisibility(View.GONE);
        else fields.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
