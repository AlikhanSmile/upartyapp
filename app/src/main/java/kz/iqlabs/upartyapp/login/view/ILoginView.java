package kz.iqlabs.upartyapp.login.view;

public interface ILoginView {
    void onLoginResult(Boolean result, int code, String errorMessage);
    void onSetProgressBarVisibility(int visibility);
}
