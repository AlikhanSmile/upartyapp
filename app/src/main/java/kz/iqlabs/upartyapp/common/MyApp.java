package kz.iqlabs.upartyapp.common;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyApp extends Application {
    public static String googleKey = "AIzaSyDUKauzJtuBv2vzn-zIFM6Alb5vma_0LSs";
    @Override
    public void onCreate() {
        super.onCreate();
        TypefaceUtil.overrideFont(getApplicationContext(), "serif", "fonts/akroblack.otf");

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration
                .Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }


}
