package kz.iqlabs.upartyapp.registration.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.opengl.Visibility;
import android.view.View;

import com.google.gson.Gson;

import kz.iqlabs.upartyapp.login.api.LoginService;
import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.registration.api.RegistrationService;
import kz.iqlabs.upartyapp.registration.model.RegistrationResponse;
import kz.iqlabs.upartyapp.registration.view.IRegistrationView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationPresenter implements IRegistrationPresenter {
    private IRegistrationView iRegistrationView;

    private RegistrationService registrationService;
    private LoginService loginService;
    private Context context;
    private String email,pass;
    public  RegistrationPresenter(IRegistrationView iRegistrationView) {
        this.iRegistrationView = iRegistrationView;
        this.context = (Context) iRegistrationView;
        registrationService = ApiClient.getClient().create(RegistrationService.class);
        loginService = ApiClient.getClient().create(LoginService.class);
    }

    @Override
    public void doRegistration(String email, String pass, String phone) {
        this.email = email;
        this.pass = pass;
        setProgressBarVisiblity(View.VISIBLE);
        registrationService.doRegistration(email,pass,phone).enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                setProgressBarVisiblity(View.GONE);
                if(response.body().getSuccess()) {
                    iRegistrationView.onRegistrationResult("doRegistration",true,response.code(),null);
                    confirmEmail();
                } else {
                    iRegistrationView.onRegistrationResult("doRegistration",false,response.code(),"The password/email has already been taken");
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                setProgressBarVisiblity(View.GONE);
                iRegistrationView.onRegistrationResult("doRegistration",false,404,"The password/email has already been taken");
                t.printStackTrace();
            }
        });

    }

    private void confirmEmail() {
        setProgressBarVisiblity(View.VISIBLE);
        registrationService.confirmEmail(email).enqueue(new Callback<RegistrationResponse>() {
            @Override
            public void onResponse(Call<RegistrationResponse> call, Response<RegistrationResponse> response) {
                setProgressBarVisiblity(View.GONE);
                if(response.body().getSuccess()) {
                    iRegistrationView.onRegistrationResult("confirmEmail",true,response.code(),null);
                    doLogin();
                } else {
                    iRegistrationView.onRegistrationResult("doRegistration",false,response.code(),"The password/email has already been taken");
                }
            }

            @Override
            public void onFailure(Call<RegistrationResponse> call, Throwable t) {
                setProgressBarVisiblity(View.GONE);
                iRegistrationView.onRegistrationResult("doRegistration",false,404,t.getMessage());
                t.printStackTrace();
            }
        });
    }

    private void doLogin() {
        setProgressBarVisiblity(View.VISIBLE);
        loginService.makeLogin(email,pass).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                setProgressBarVisiblity(View.GONE);
                String errorMessage = null;
                if(response.isSuccessful() && response.body().getSuccess()) {
                    UserModel userModel = response.body().getUserModel();
                    userModel.setPassword(pass);
                    String json = new Gson().toJson(userModel);
                    SharedPreferences sharedPreferences = context.getSharedPreferences("",0);
                    sharedPreferences.edit().putString("user",json).apply();
                } else {
                    errorMessage = response.body().getErrors().get(0).getMessage();
                }
                iRegistrationView.onRegistrationResult("doLogin",response.body().getSuccess(),response.code(),errorMessage);

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                setProgressBarVisiblity(View.GONE);
                iRegistrationView.onRegistrationResult("doRegistration",false,404,t.getMessage());
                t.printStackTrace();
            }
        });
    }

    @Override
    public void setProgressBarVisiblity(int visibility) {
        iRegistrationView.onSetProgressBarVisibility(visibility);
    }
}
