package kz.iqlabs.upartyapp.registration.presenter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import kz.iqlabs.upartyapp.login.api.LoginService;
import kz.iqlabs.upartyapp.login.model.LoginResponse;
import kz.iqlabs.upartyapp.login.model.UserModel;
import kz.iqlabs.upartyapp.network.ApiClient;
import kz.iqlabs.upartyapp.registration.api.RegistrationService;
import kz.iqlabs.upartyapp.registration.view.IPickRoleView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PickRolePresenter implements IPickRolePresenter {

    private Context context;
    private IPickRoleView iPickRoleView;

    private RegistrationService registrationService;
    private LoginService loginService;

    private String user_id,email,pass;
    public PickRolePresenter(IPickRoleView iPickRoleView) {
        this.context = (Context) iPickRoleView;
        this.iPickRoleView = iPickRoleView;
        registrationService = ApiClient.getClient().create(RegistrationService.class);
    }


    @Override
    public void updateUser(String name, String filePath, String about, String state, String city, String role) {

        String userJson = context.getSharedPreferences("", 0).getString("user", "");
        UserModel userModel = new Gson().fromJson(userJson, UserModel.class);
        user_id = String.valueOf(userModel.getId());
        email = userModel.getEmail();
        pass = userModel.getPassword();

        File file = new File(filePath);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("avatar", file.getName(), requestFile);

        String url = "http://iqlabs.kz/uparty/public/api/user/update/"+user_id;
        registrationService.updateUser(url,name,state,city,about,body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body()!=null) {
                    try {

                        iPickRoleView.onRequestResult("updateUser", response.isSuccessful(), response.code(), response.body().string());
                        setRole(role);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else
                    iPickRoleView.onRequestResult("updateUser", response.isSuccessful(), response.code(), null);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                iPickRoleView.onRequestResult("updateUser", false, 404, t.getMessage());
                t.printStackTrace();
            }
        });



    }

    private void setRole(String role) {
        String url = "http://iqlabs.kz/uparty/public/api/user/set-role/"+user_id;
        registrationService.setRole(url,role).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                doLogin();
                iPickRoleView.onRequestResult("setRole", response.isSuccessful(), response.code(), null);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                iPickRoleView.onRequestResult("setRole", false, 404, null);
            }
        });
    }


    private void doLogin() {
        //setProgressBarVisiblity(View.VISIBLE);
        loginService = ApiClient.getClient().create(LoginService.class);
        loginService.makeLogin(email,pass).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
              //  setProgressBarVisiblity(View.GONE);
                String errorMessage = null;
                if(response.isSuccessful() && response.body().getSuccess()) {
                    UserModel userModel = response.body().getUserModel();
                    userModel.setPassword(pass);
                    String json = new Gson().toJson(userModel);
                    SharedPreferences sharedPreferences = context.getSharedPreferences("",0);
                    sharedPreferences.edit().putString("user",json).apply();
                } else {
                    errorMessage = response.body().getErrors().get(0).getMessage();
                }
                iPickRoleView.onRequestResult("doLogin",response.body().getSuccess(),response.code(),errorMessage);

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                setProgressBarVisiblity(View.GONE);
                iPickRoleView.onRequestResult("doLogin",false,404,"The phone/email has already been taken");
                t.printStackTrace();
            }
        });
    }

    @Override
    public void setProgressBarVisiblity(int visibility) {
//        setProgressBarVisiblity(visibility);
    }
}
