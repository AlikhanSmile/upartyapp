package kz.iqlabs.upartyapp.registration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.github.vacxe.phonemask.PhoneMaskManager;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.login.LoginActivity;
import kz.iqlabs.upartyapp.login.presenter.LoginPresenter;
import kz.iqlabs.upartyapp.registration.presenter.RegistrationPresenter;
import kz.iqlabs.upartyapp.registration.view.IRegistrationView;

public class RegistrationActivity extends AppCompatActivity implements IRegistrationView {

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.confirm_password)
    EditText confirm_password;
    @BindView(R.id.fields)
    LinearLayout fields;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.phone)
    EditText phone;


    RegistrationPresenter registrationPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        init();
    }
    private void init() {
        new PhoneMaskManager().withMask(" (###) ###-##-##").withRegion("+1").bindTo(phone);
        registrationPresenter = new RegistrationPresenter(this);
    }
    @OnClick(R.id.login)
    void goLogin() {
        startActivity(new Intent(this, LoginActivity.class));
    }

    @OnClick(R.id.signup)
    void goPickRole() {
        if(!emptyParams()) {
            if(passOk()) {
                registrationPresenter.doRegistration(email.getText().toString(),password.getText().toString(),rawPhone());
            } else {
                Snackbar.make(email,"Passwords are different", Snackbar.LENGTH_SHORT).show();
            }
        } else {
            Snackbar.make(email,"All fields should be filled", Snackbar.LENGTH_SHORT).show();
        }
    }

    private String rawPhone() {
        String str = phone.getText().toString();
        str = str.replaceAll("[^\\d.]", "");
        str = str.substring(1);
        return str;
    }

    private boolean emptyParams() {
        return email.getText().toString().isEmpty() ||
                password.getText().toString().isEmpty() ||
                confirm_password.getText().toString().isEmpty() ||
                phone.getText().toString().isEmpty();
    }

    private boolean passOk() {
        if(!emptyParams()) {
            return password.getText().toString().equals(confirm_password.getText().toString());
        } else {
            return false;
        }
    }

    @Override
    public void onRegistrationResult(String method,Boolean result, int code, String errorMessage) {
        if(method.equals("doLogin") && result) {
            Intent intent = new Intent(this,BasicInfoActivity.class);
            startActivity(intent);
        }

        if(!result) Snackbar.make(email,errorMessage,Snackbar.LENGTH_SHORT).show();

        Log.d(method,result+" "+code+" "+errorMessage);
    }

    @Override
    public void onSetProgressBarVisibility(int visibility) {
        progressBar.setVisibility(visibility);
        if(visibility== View.VISIBLE) {
            fields.setVisibility(View.GONE);
        } else {
            fields.setVisibility(View.VISIBLE);
        }
    }
}
