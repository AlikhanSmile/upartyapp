package kz.iqlabs.upartyapp.registration.view;

public interface IRegistrationView {
    void onRegistrationResult(String method,Boolean result, int code, String errorMessage);
    void onSetProgressBarVisibility(int visibility);
}
