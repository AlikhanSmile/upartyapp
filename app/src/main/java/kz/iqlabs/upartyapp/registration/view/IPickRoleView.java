package kz.iqlabs.upartyapp.registration.view;

public interface IPickRoleView {
    void onRequestResult(String method,Boolean result, int code, String errorMessage);
    void onSetProgressBarVisibility(int visibility);
}
