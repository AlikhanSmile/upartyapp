package kz.iqlabs.upartyapp.registration.api;

import kz.iqlabs.upartyapp.registration.model.RegistrationResponse;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RegistrationService {
    @POST("auth/register")
    @FormUrlEncoded
    Call<RegistrationResponse> doRegistration(@Field("email") String email,
                                              @Field("password") String pass,
                                              @Field("phone") String phone);

    @POST("auth/confirm")
    @FormUrlEncoded
    Call<RegistrationResponse> confirmEmail(@Field("email") String email);



    @Multipart
    @POST
    Call<ResponseBody> updateUser(@Url String url,
                                  @Query("name") String name,
                                  @Query("state") String state,
                                  @Query("city") String city,
                                  @Query("about") String about,
                                  @Part MultipartBody.Part avatar);

    @POST
    Call<ResponseBody> setRole(@Url String url,
                               @Query("role") String role);


}
