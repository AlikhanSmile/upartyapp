package kz.iqlabs.upartyapp.registration;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.iqlabs.upartyapp.BuildConfig;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.common.AppHelper;
import kz.iqlabs.upartyapp.main.MainActivity;
import kz.iqlabs.upartyapp.registration.presenter.PickRolePresenter;
import kz.iqlabs.upartyapp.registration.view.IPickRoleView;

import static android.media.MediaRecorder.VideoSource.CAMERA;

public class PickRoleActivity extends AppCompatActivity implements IPickRoleView {

    @BindView(R.id.almost_done)
    TextView almost_done;

    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    SharedPreferences sharedPreferences;
    String full_name, first_name, last_name, city, state, filePath, about, role;

    PickRolePresenter pickRolePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_role);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        pickRolePresenter = new PickRolePresenter(this);
        sharedPreferences = getSharedPreferences("", 0);
        Typeface akroblack = Typeface.createFromAsset(getAssets(), "fonts/akroblack.otf");
        almost_done.setTypeface(akroblack);
        first_name = getIntent().getExtras().getString("first_name");
        last_name = getIntent().getExtras().getString("last_name");
        full_name = first_name + " " + last_name;
        city = getIntent().getExtras().getString("city");
        state = "XX";
        about = "There is no information";
        checkPermissions();

        Log.d("first_name", first_name);
        Log.d("last_name", last_name);
        Log.d("city", city);
        Log.d("about", about);
    }



    @OnClick(R.id.employee)
    void goEmployee() {
        role = "employee";
        pickRolePresenter.updateUser(full_name, filePath, about, state, city, role);

    }

    @OnClick(R.id.employer)
    void goEmployer() {
        role = "employer";
        pickRolePresenter.updateUser(full_name, filePath, about, state, city, role);
    }


    @Override
    public void onRequestResult(String method, Boolean result, int code, String errorMessage) {
        Log.d(method, result + " " + code + " " + errorMessage);
        if(method.equals("doLogin") && result) {
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onSetProgressBarVisibility(int visibility) {
        progressBar.setVisibility(visibility);

    }


    //notused

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1052: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    filePath = AppHelper.defaultAvatarPath(this);
                    // permission was granted.

                } else {


                    // Permission denied - Show a message to inform the user that this app only works
                    // with these permissions granted

                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_CANCELED) {
            switch (requestCode) {
                case 0:
                    if (resultCode == RESULT_OK && data != null) {
                        Bitmap selectedImage = (Bitmap) data.getExtras().get("data");
                        imageView.setImageBitmap(selectedImage);


                    }

                    break;
                case 1:
                    if (resultCode == RESULT_OK && data != null) {
                        Uri selectedImage = data.getData();
                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
                        if (selectedImage != null) {
                            Cursor cursor = getContentResolver().query(selectedImage,
                                    filePathColumn, null, null, null);
                            if (cursor != null) {
                                cursor.moveToFirst();

                                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                                String picturePath = cursor.getString(columnIndex);
                                imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                                cursor.close();


                            }
                        }

                    }
                    break;
            }
        }
    }

    private void checkPermissions() {


        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA
                    },
                    1052);

        } else {
            filePath = AppHelper.defaultAvatarPath(this);
        }
    }

    private void selectImage(Context context) {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Choose your profile picture");

        builder.setItems(options, (dialog, item) -> {

            if (options[item].equals("Take Photo")) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);

            } else if (options[item].equals("Choose from Gallery")) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);

            } else if (options[item].equals("Cancel")) {
                dialog.dismiss();
            }
        });
        builder.show();
    }




}
