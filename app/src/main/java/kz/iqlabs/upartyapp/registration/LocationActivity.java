package kz.iqlabs.upartyapp.registration;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.snackbar.Snackbar;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.iqlabs.upartyapp.R;
import kz.iqlabs.upartyapp.common.MyApp;

public class LocationActivity extends AppCompatActivity {

    @BindView(R.id.basic_text)
    TextView basic_text;

    @BindView(R.id.city)
    EditText city;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        Typeface akroblack = Typeface.createFromAsset(getAssets(), "fonts/akroblack.otf");
        basic_text.setTypeface(akroblack);

    }


    @OnClick(R.id.next)
    void nextClick() {
        if (city.getText().toString().isEmpty()) {
            Snackbar.make(basic_text, "Please choose your city", Snackbar.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(this, PickRoleActivity.class);
        intent.putExtra("first_name", getIntent().getExtras().getString("first_name"));
        intent.putExtra("last_name", getIntent().getExtras().getString("last_name"));
        intent.putExtra("city", city.getText().toString());
        startActivity(intent);
    }
}
