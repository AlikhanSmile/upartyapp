package kz.iqlabs.upartyapp.registration.presenter;

public interface IRegistrationPresenter {
    void doRegistration(String email, String pass,String phone);
    void setProgressBarVisiblity(int visibility);
}
