package kz.iqlabs.upartyapp.registration.presenter;

public interface IPickRolePresenter {
    void updateUser(String name, String filePath,String about,String state, String city, String role);
    void setProgressBarVisiblity(int visibility);
}
