package kz.iqlabs.upartyapp.registration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.iqlabs.upartyapp.R;

public class BasicInfoActivity extends AppCompatActivity {

    @BindView(R.id.basic_text)
    TextView basic_text;

    @BindView(R.id.first_name)
    EditText first_name;
    @BindView(R.id.last_name)
    EditText last_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_info);
        ButterKnife.bind(this);
        Typeface akroblack = Typeface.createFromAsset(getAssets(), "fonts/akroblack.otf");
        basic_text.setTypeface(akroblack);
    }

    @OnClick(R.id.next)
    void nextClick() {
        if(!emptyCheck()) {
            Intent intent = new Intent(this,LocationActivity.class);
            intent.putExtra("first_name",first_name.getText().toString());
            intent.putExtra("last_name",last_name.getText().toString());
            startActivity(intent);
        } else {
            Snackbar.make(first_name,"All fields are required", Snackbar.LENGTH_SHORT).show();
        }

    }

    private boolean emptyCheck() {
        return first_name.getText().toString().isEmpty() || last_name.getText().toString().isEmpty();
    }
}